
% Required packages
pkg load geometry;

% Dipoles
r = [0.,0.];   % Location
p = [0.2,0.2]; % Moment

% Plot
plot_potential_field(r,p,[-1,1,-1,1]);
