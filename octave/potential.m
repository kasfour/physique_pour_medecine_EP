function V = potential(p,r_0,x,y)
% function V = potential(p,r_0,x,y)
%
% Computes the potential field V(r) at the point r=[x,y] of the family of
% dipoles p located at points r_0. It is given by the formula:
%
% V(r) = sum( p.(r-r_0) / |r-r_0|^3 ).
%
% Input: 
% p : array of size Nx2 of the moments of the dipoles (there are N dipoles)
% r_0 : locations of the N dipoles, array of size Nx2 of the coordinates
%       x_0,y_0
% x,y : arrays of coordinates x,y of any size.
%
% Output:
% V : array of the values of the potential. It has the shape and size of x
%     and y.

% Find the number of dipoles
N = size(p,1);

% Allocate memory for V inone time (fastest)
V = 0.*x;

% Loop over the dipoles
for i = [1:N]
    r = sqrt( (x-r_0(i,1)).^2 + (y-r_0(i,2)).^2 );
    V = V + (p(i,1)*(x-r_0(i,1)) + p(i,2)*(y-r_0(i,2))) ./ r.^3;
end

