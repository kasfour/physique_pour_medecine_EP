function [t,y] = rlk(m,t0,h,n, k)
% Solve the ionic_model m starting from time t0, with the time-step h and n
% iterations. The optional last parameter is the order of the method (currently
% k = 1 or 2).
    
    % Initial data -- initialize first two steps since we used a two steps method
    t = [0:h:n*h];
    y = zeros(get(m,"n_vars"),1+n);
    y(:,1) = get(m,"steady_state");
    y(:,2) = get(m,"steady_state");
    
    for i = 2:n
        [An,  Bn  ] = AB(m,t(i),  y(:,i)  );
        [Anmu,Bnmu] = AB(m,t(i-1),y(:,i-1));
        alpha = 1.5*An - 0.5*Anmu;
        beta = 1.5*Bn - 0.5*Bn;
        % This is correct and easy to compute because alpha is a diagonal matrix
        y(:,1+i) = y(:,i) + h*phi_1(h*alpha).*(alpha.*y(:,i)+beta);
    end
