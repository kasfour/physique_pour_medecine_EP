
% Required packages
pkg load geometry;

% We want to have a model in which a dipole moves along a path. The path is
% given by functions of time (coordinates), and the dipole is assumed to alway
% be aligned with the tangent of the line.

% Loop around a fixed point
omega = 1.; % Velocity is 1 loop/time unit
f = inline("[0,0]","t");
df = inline("[cos(2*pi*omega*t), sin(2*pi*omega*t)]","t");
amplitude= inline("1.","t");  % Amplitude as a function of time
t_final = 2; % Go for 2 loops
dt = 0.02;                    % Time step in s

% Window in wich to observe the phenomena
window = [-1,1,-1,1]; % xmin,xmax,ymin,ymax

% Points where we want to record the potential
xV = [-0.1,0; 0.1,0; -0.1,0.1; 0.1,0.1];

% Animate the potential field
figure(1); clf;
[t,Vs] = plot_moving_dipole(f,df,amplitude,window,t_final,dt,xV);

% Now plot the recorded potentials
figure(2);
plot(t,Vs(2,:)-Vs(1,:),'r-','linewidth',2,...
     t,Vs(4,:)-Vs(3,:),'b-','linewidth',2);
legend('V2-V1','V4-V3');
title('Recorded voltages')
xlabel('time');
ylabel('voltage');
grid;
% print -dpng "V_loop.png"; % Save the graph to a png file
