function [t,Vs] = plot_moving_dipole(f,df,amplitude,window,t_final,dt,Pts, file_name)
% function [t,Vs] = plot_moving_dipole(f,df,amplitude,window,t_final,dt,Pts[, file_name])
%
% Plot the electrical field associated to a single dipole moving along a
% path. The dipole is assumed to be always aligned with the path, and with a
% fixed normalized amplitude.
%
% f, df: function R-->R^2 that define the Path and its derivative
% amplitude: functionR-->R^+ that defines the amplitude
% window: [wmin,wmax,ymin,ymax], window to draw the plot
% t_final: final time (starting from 0)
% dt: time step between to frames
% Pts: coordinates of the points where a potential has to be recorded along
%      time, one point per row [x1;x2;x3...]
% file_name: optional root of the sequence of files to which the plits will be
%            saved.
Ni = ceil(t_final/dt); % Number of iteration to go from r0 to r1.
t = [0:dt:Ni*dt];

% Initialise the arrays for the V and t
Npts = size(Pts,1);
Vs = zeros(Npts,1+Ni);

for i = 0:Ni

    r = f(t(1+i));  % Current location
    n = df(t(1+i)); % Derivative = Tangent to the path
    p = amplitude(t(1+i))*n/norm(n); % Dipole with normalized amplitude
    
    plot_potential_field(r,p,window); % Update the plot of the potential field
    for k = 1:Npts
        text(Pts(k,1),Pts(k,2),num2str(k),"color","red","fontsize",12);
        Vs(k,1+i) = potential(p,r,Pts(k,1),Pts(k,2));
    end
    axis('square'); xlabel('x'),ylabel('y');
    hold off;
    title (sprintf("Time t = %.2f",t(1+i)));

    if nargin==8
        fn=sprintf('%s_%05d.png',file_name,i);
        print(fn);
    end
    
    pause(1.e-6);
end
