
% Required packages
pkg load geometry;

% We want to have a model in which a dipole moves along a path. The path is
% given by functions of time (coordinates), and the dipole is assumed to alway
% be aligned with the tangent of the line.

% Loop around a circle
r = 0.5; % Radius
omega = 1.; % Velocity is 1 loop/time unit
f = inline("r*[cos(2*pi*omega*t), sin(2*pi*omega*t)]","t"); % A circle
df = inline("r*2*pi*[-sin(2*pi*omega*t), cos(2*pi*omega*t)]","t");
amplitude= inline("1.","t");  % Amplitude as a function of time
t_final = 2; % Go for 2 loops
dt = 0.02; % Time step in s

% Window in wich to observe the phenomena
window = [-1,1,-1,1]; % xmin,xmax,ymin,ymax

% Points where we want to record the potential
xV = [-0.75,0; 0.75,0; 0,0];

% Animate the potential field
figure(1); clf;
[t,Vs] = plot_moving_dipole(f,df,amplitude,window,t_final,dt,xV);

% Now plot the recorded potentials
figure(2); clf;
plot(t,Vs(2,:)-Vs(1,:),'k-', 'linewidth',2, ...
     t,Vs(1,:)-Vs(3,:),'r-', 'linewidth',2, ...
     t,Vs(2,:)-Vs(3,:),'b-', 'linewidth',2);
legend('V2-V1','V1-V3','V2-V3');
title('Recorded voltages')
xlabel('time');
ylabel('voltage');
grid;
%print -dpng "V_circulate.png"; % Save the graph to a png

