function y = phi_1(z)
% Compute the function phi_1(z) := (exp(z)-1)/z prolongated by 0 in 0.
    y = z*0.;
    y(find(abs(z)<=1.e-6)) = 1. + 0.5*z(find(abs(z)<=1.e-6)) ...
        + z(find(abs(z)<=1.e-6)).^2/6;
    y(find(abs(z)>1.e-6)) = ( exp(z(find(abs(z)>1.e-6))) - 1. ) ...
        ./ z(find(abs(z)>1.e-6));
