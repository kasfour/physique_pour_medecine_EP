ms = ionic_model('MS');

ms = set_stimulation(ms,[10,.301,1000]);
[t4,y4] = rlk(ms,0.,1.e-1,500*10);

ms = set_stimulation(ms,[10,.300,1000]);
[t3,y3] = rlk(ms,0.,1.e-1,500*10);

ms = set_stimulation(ms,[10,1.,1000]);
[t2,y2] = rlk(ms,0.,1.e-1,500*10);

ms = set_stimulation(ms,[10,5.,1000]);
[t1,y1] = rlk(ms,0.,1.e-1,500*10);

figure(1);
plot(t1,y1(2,:),'k-','linewidth',2,...
     t4,y4(2,:),'b-','linewidth',2,...
     t3,y3(2,:),'r-','linewidth',2);
grid;
legend('current applied for 5.000 ms',...
       'current applied for 0.301 ms',...
       'current applied for 0.300 ms');
xlabel('t (ms)');
ylabel('V (normalized)');
title('Mitchell-Schaeffer ionic model');
print('MS_onOff.png');
