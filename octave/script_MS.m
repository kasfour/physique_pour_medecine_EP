
% Define a MS ionic model with default parameters
ms = ionic_model('MS');

% We will run the MSmodel with cycke length of : 
CL = [600,500,400,300,250,200,150,100];

for i = 1:length(CL)
    % Set the stimulation
    ms = set_stimulation(ms,[10,1.,CL(i)]);
    % Run the simulation
    [t,y] = rlk(ms,0.,1.e-1,10+10*6*CL);
    figure(i,"visible","off");
    plot(t,y(1,:),'b-',"linewidth",2, ...
         t,y(2,:),'r-',"linewidth",2);
    axis([0,t(end),0,1]);
    legend("h","V");
    title(sprintf("MS action voltage  -- CL=%.1f",CL(i)))
    xlabel("t (ms)");
    ylabel("V (normalized)");
    grid;
    print(sprintf("MS_CL%d.png",CL(i)));
    display(sprintf("Finished CL=%d",CL(i)));
end
