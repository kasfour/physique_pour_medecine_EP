function plot_potential_field(r_0,p,window)
% function plot_potential_field(r_0,p,window)
%
% Plot the contour lines of the potential field associated to a finite family of
% dipoles p located at points r_0. The plot is limited to the window given as
% the last argument.
%
% r_0: array of size Nx2 of the locations of the dipoles, [x1,y1; x2,y2...]
% p: array of size Nx2 of the dipole moments, [p1x,p1y; p2x,p2y...]
% window: array of size 4, [xmin,xmax, ymin,ymax]

N = size(p,1); % Number of dipoles

% Potential field
xmin = window(1); xmax = window(2);
ymin = window(3); ymax = window(4);

% Number of divisions in each direction for the plot
Nx = 30;
Ny = 30;

% First, create a grid of points x,y. Here, x and y will be matrices such that
% x_{ji} = x_min + i*hx and y_{ji} = y_min + j*hy, where hx = (x_max-x_min)/Nx,
% and hy = (y_max-y_min)/Ny
[x,y] = meshgrid(linspace(xmin,xmax,Nx+1),linspace(ymin,ymax,Ny+1));

% Now compute the potential field (see potential.m)
V = potential(p,r_0,x,y);

% Finally draw the contour lines of the field V
%cValues = [-20:2:-2, -2:0.2:-0.2, 0.2:0.2:2, 2:2:20]; % Values at which a
%                                                      % contour line is drawn
cValues = [-20:2:20];
[c,h] = contour(x,y,V,cValues,'linewidth',2);         % plot the contour lines
                                        % clabel (c, h, cValues, "fontsize", 12); % label the lines
hold on;
% Draws arrows representing the dipoles
scale = 0.5; % Scale used to represent the dipoles
arrows = [r_0 r_0+scale*p];
ht = 0.25*ones(1,N);
drawArrow (arrows, 1, 0.025 , 0.025, ht);
% Legend and title
%xlabel('x');
%ylabel('y');
axis(window);

