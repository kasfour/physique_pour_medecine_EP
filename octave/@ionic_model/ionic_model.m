function model = ionic_model(name)
    if (nargin == 0)
        name = "MS"
    end
    model.implemented_models = {"MS","BR","TNNP"};
    test = strcmp(name,model.implemented_models);
    if all(test==0)
        error ("ionic_model: unknown model name.");
    end
    model.i = find(test==1); % Index of the model
    switch (model.i)
      case 1 % MS
        model.tau_in    =   0.3; % ms
        model.tau_out   =   6.;  % ms
        model.tau_open  = 120.;  % ms
        model.tau_close = 150.;  % ms
        model.v_gate = 0.13;
        model.n_gates = 1;
        model.n_misc = 0;
        model.n_vars = model.n_gates + model.n_misc + 1;
        model.n_currents = 1;
        model.steady_state = [1.;0.];
        model.stimulation = [10.,1.,500.]; % For each stimulation, offset,
                                           % duration, period. Amplitude is
                                           % fixed
      otherwise
        error ("ionic_model: model not implemented yet.");
    end
    
    model = class(model, "ionic_model");
    
