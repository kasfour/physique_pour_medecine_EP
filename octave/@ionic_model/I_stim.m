function I = I_stim(self,t)
% I = stim(self,t)
% Stimulation currents for the model

I = 0.*t;

switch (self.i)
  case 1 % MS
    for i = 1:size(self.stimulation,1) % 1 stimulation per row
        offset = self.stimulation(i,1);
        dt = self.stimulation(i,2);
        period = self.stimulation(i,3);
        t0 = rem(t-offset, period);
        I(find(0.<=t0 & t0<=dt)) = 0.15;
    end
  otherwise
    error ("ionic_model: model not implemented yet.");
end
