function display(self)
fprintf ("%s =\n", inputname(1));
switch (self.i)
  case 1 % MS
    fprintf("  ionic model %s with \n",self.implemented_models{self.i});
    fprintf("  tau_in = %f\n",self.tau_in);
    fprintf("  tau_out = %f\n",self.tau_out);
    fprintf("  tau_open = %f\n",self.tau_open);
    fprintf("  tau_close = %f\n",self.tau_close);
    fprintf("  v_gate = %f\n",self.v_gate);
    for i = 1:size(self.stimulation)
        fprintf("  stimulation #%02d\n",i);
        fprintf("    offset : %.1f\n",self.stimulation(i,1));
        fprintf("    duration : %.1f\n",self.stimulation(i,2));
        fprintf("    period : %.1f\n",self.stimulation(i,3));
    end        
  otherwise
    fprintf("This model is not implemented yet.\n");
end
