function [A,B] = AB(self,t,Y)
% Each model is written in the form Y' = A(Y)*Y + B(Y), so as to use exponential
% methods, such as the RLk. The unknown is assumed to be order as Y = (W,X,V)
% where W are the n_gates gating variables, X are the n_misc auxiliary
% variables, and V is the transmembranec voltage. The RHs is composed of the
% diagonal matrix A(Y), represented by its diagonal only, and the vector
% B(Y).
%
% In practise, A(Y) = [-tau^{-1} 0] and B(Y) = [tau^{-1}*W_infty]
%                     [ 0        0]            [F(Y)]
% where F(Y) = [G(Y), I_ion(Y) + stim(t)], and G(Y) are the evolution rates of
% the auxiliary variables, and I_ion is the total ionic current.

% Assume that Y is possibly a matrix of values of size n_vars x n_values
n_vars = size(Y,1);
n_values = size(Y,2);
if (n_vars ~= self.n_vars)
    error ("The unknown vector Y does not have the right number of variables");
end

A = zeros(n_vars,n_values);
B = zeros(n_vars,n_values);

    switch (self.i)
      case 1 % MS
        % First, let's compute the W_inf and tau
        tau = zeros(1,n_values);
        w_inf = zeros(1,n_values);
        
        w_inf(Y(n_vars,:)<self.v_gate) = 1.; % 0 elsewhere
        w_inf(Y(n_vars,:)>=self.v_gate) = 0.; % 0 elsewhere
        
        tau(Y(n_vars,:)<self.v_gate) = self.tau_open;
        tau(Y(n_vars,:)>=self.v_gate) = self.tau_close;
        
        % We can compute the first line of A and B
        A(1,:) = -1./tau;
        B(1,:) = w_inf./tau;
        
        % Then the second line of B, 
        % B(2,:) := I_ion + stim(t) = h*v^2*(1-v)/tau_in - v/tau_out + stim(t)
        B(2,:) = I_stim(self,t) + ...
                 Y(1,:) .* Y(2,:)**2 .* (1.-Y(2,:)) / self.tau_in - Y(2,:) / self.tau_out;
      otherwise
        error ("ionic_model: model not implemented yet.");
    end
