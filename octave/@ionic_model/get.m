function s = get (self, f)
if (nargin == 1)
    s = struct(self);
elseif (nargin == 2)
    if (ischar (f))
        self_struct = struct(self);
        if (isfield(self_struct,f))
            s = self_struct.(f);
        else
            error ("get: invalid property %s", f);
        end
    else
        error ("get: expecting the property to be a string");
    end
else
    print_usage ();
end

