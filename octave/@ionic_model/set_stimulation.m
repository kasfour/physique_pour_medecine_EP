function self = set_stimulation(self,stim)
% set_stimulation(self,stim)
% Set the stimulation currents for the model

self.stimulation = stim;
