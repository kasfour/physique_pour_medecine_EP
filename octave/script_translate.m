
% Required packages
pkg load geometry;

% We want to have a model in which a dipole moves along a path. The path is
% given by functions of time (coordinates), and the dipole is assumed to alway
% be aligned with the tangent of the line.

% Exemple for a translation between two points
r0 = [-1.,0.]; % Startpoint
r1 = [+1.,0.]; % Endpoint
c = 1.; %Velocity in length unit/s
n = (r1-r0)/norm(r1-r0); % Direction of propagation
                         
% Functions that define the path
f = inline("r0 + c*t*n","t"); % The path as a function of "t", the other
                              % parameters are fixed.
df = inline("c*n","t");       % Derivative of the path
amplitude= inline("1.","t");  % Amplitude as a function of time
t_final = norm(r1-r0)/c;      % Time to reach the endpoint
dt = 0.02;                    % Time step in s

% Window in wich to observe the phenomena
window = [-1,1, 0.,0.25]; % xmin,xmax,ymin,ymax

% Points where we want to record the potential
xV = [-0.01,0.1; 0.01,0.1];

% Animate the potential field
figure(1); clf;
[t,Vs] = plot_moving_dipole(f,df,amplitude,window,t_final,dt,xV);

% Now plot the recorded potentials
figure(2);
plot(t,Vs(1,:),'k-', 'linewidth',2, ...
     t,Vs(1,:)-Vs(2,:),'r-', 'linewidth',2);
legend('monopolaire','dipolaire');
title('Recorded voltages')
xlabel('time');
ylabel('voltage');
grid;
% print -dpng "V_translate.png" % Save the graph to a png file
